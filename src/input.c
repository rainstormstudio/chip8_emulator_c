#include "input.h"

static void input_clear(input_t* self) {
    size_t i;
    for (i = 0; i < KEYPAD_SIZE; i ++) {
        self->keys[i] = false;
    }
    self->has_input = false;
}

static void input_update(input_t* self) {
    self->has_input = false;
    while (sfRenderWindow_pollEvent(self->window, &self->event)) {
        if (self->event.type == sfEvtClosed) {
            sfRenderWindow_close(self->window);
        } else if (self->event.type == sfEvtKeyPressed ||
                   self->event.type == sfEvtKeyReleased) {
            bool pressed = (self->event.type == sfEvtKeyPressed) ? true : false;
            self->has_input = pressed;

            switch (self->event.key.code) {
            case sfKeyNum1: { self->keys[KEY_1] = pressed; break; }
            case sfKeyNum2: { self->keys[KEY_2] = pressed; break; }
            case sfKeyNum3: { self->keys[KEY_3] = pressed; break; }
            case sfKeyNum4: { self->keys[KEY_4] = pressed; break; }
            case sfKeyQ: { self->keys[KEY_Q] = pressed; break; }
            case sfKeyW: { self->keys[KEY_W] = pressed; break; }
            case sfKeyE: { self->keys[KEY_E] = pressed; break; }
            case sfKeyR: { self->keys[KEY_R] = pressed; break; }
            case sfKeyA: { self->keys[KEY_A] = pressed; break; }
            case sfKeyS: { self->keys[KEY_S] = pressed; break; }
            case sfKeyD: { self->keys[KEY_D] = pressed; break; }
            case sfKeyF: { self->keys[KEY_F] = pressed; break; }
            case sfKeyZ: { self->keys[KEY_Z] = pressed; break; }
            case sfKeyX: { self->keys[KEY_X] = pressed; break; }
            case sfKeyC: { self->keys[KEY_C] = pressed; break; }
            case sfKeyV: { self->keys[KEY_V] = pressed; break; }
            default: { break; }
            }
        }
    }
}

input_t* input_new(sfRenderWindow* window) {
    input_t* input = (input_t*)malloc(sizeof(input_t)); {
        input->window = window;
        input->has_input = false;
        input_clear(input);
        input->update = input_update;
        input->key_pressed = input_key_pressed;
    }
    return input;
}

void input_delete(input_t** input) {
    free(*input);
}
