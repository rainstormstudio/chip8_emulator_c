#include "cpu.h"

static void cpu_load_font_sprites(cpu_t* self) {
    const u8 fonts[FONTS_SIZE] = {
        0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
        0x20, 0x60, 0x20, 0x20, 0x70, // 1
        0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
        0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
        0x90, 0x90, 0xF0, 0x10, 0x10, // 4
        0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
        0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
        0xF0, 0x10, 0x20, 0x40, 0x40, // 7
        0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
        0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
        0xF0, 0x90, 0xF0, 0x90, 0x90, // A
        0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
        0xF0, 0x80, 0x80, 0x80, 0xF0, // C
        0xE0, 0x90, 0x90, 0x90, 0xE0, // D
        0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
        0xF0, 0x80, 0xF0, 0x80, 0x80  // F
    };
    size_t i;
    for (i = 0; i < FONTS_SIZE; i ++) {
        // fonts are stored in the memory starting at 0x00
        self->memory[0x00 + i] = fonts[i];
    }
}

void cpu_load_program(cpu_t* self, Array(u8)* program) {
    size_t i;
    for (i = 0; i < program->size; i ++) {
        self->memory[0x200 + i] = program->data[i];
    }
}

void cpu_init_OP_tables(cpu_t* self) {
    ARRAY_SET_ALL(self->OP_table0, OP_TABLE0_SIZE, OP_NULL);
    ARRAY_SET_ALL(self->OP_table8, OP_TABLE8_SIZE, OP_NULL);
    ARRAY_SET_ALL(self->OP_tableE, OP_TABLEE_SIZE, OP_NULL);
    ARRAY_SET_ALL(self->OP_tableF, OP_TABLEF_SIZE, OP_NULL);
    ARRAY_SET_ALL(self->OP_table, OP_TABLE_SIZE, OP_NULL);

    self->OP_table0[0x0] = OP_00E0;
    self->OP_table0[0xE] = OP_00EE;

    self->OP_table8[0x0] = OP_8xy0;
    self->OP_table8[0x1] = OP_8xy1;
    self->OP_table8[0x2] = OP_8xy2;
    self->OP_table8[0x3] = OP_8xy3;
    self->OP_table8[0x4] = OP_8xy4;
    self->OP_table8[0x5] = OP_8xy5;
    self->OP_table8[0x6] = OP_8xy6;
    self->OP_table8[0x7] = OP_8xy7;
    self->OP_table8[0xE] = OP_8xyE;

    self->OP_tableE[0x1] = OP_ExA1;
    self->OP_tableE[0xE] = OP_Ex9E;

    self->OP_tableF[0x07] = OP_Fx07;
    self->OP_tableF[0x0A] = OP_Fx0A;
    self->OP_tableF[0x15] = OP_Fx15;
    self->OP_tableF[0x18] = OP_Fx18;
    self->OP_tableF[0x1E] = OP_Fx1E;
    self->OP_tableF[0x29] = OP_Fx29;
    self->OP_tableF[0x33] = OP_Fx33;
    self->OP_tableF[0x55] = OP_Fx55;
    self->OP_tableF[0x65] = OP_Fx65;

    self->OP_table[0x0] = table0;
    self->OP_table[0x1] = OP_1nnn;
    self->OP_table[0x2] = OP_2nnn;
    self->OP_table[0x3] = OP_3xkk;
    self->OP_table[0x4] = OP_4xkk;
    self->OP_table[0x5] = OP_5xy0;
    self->OP_table[0x6] = OP_6xkk;
    self->OP_table[0x7] = OP_7xkk;
    self->OP_table[0x8] = table8;
    self->OP_table[0x9] = OP_9xy0;
    self->OP_table[0xA] = OP_Annn;
    self->OP_table[0xB] = OP_Bnnn;
    self->OP_table[0xC] = OP_Cxkk;
    self->OP_table[0xD] = OP_Dxyn;
    self->OP_table[0xE] = tableE;
    self->OP_table[0xF] = tableF;
}

static f64 delta_time(struct timespec* time_a, struct timespec* time_b) {
    f64 time_taken = (time_b->tv_sec - time_a->tv_sec) * 1e9;
    time_taken = (time_taken + (time_b->tv_nsec - time_a->tv_nsec)) * 1e-9;
    return time_taken;
}

void cpu_cycle(cpu_t* self) {
    clock_gettime(CLOCK_MONOTONIC, &self->time_b);
    while (delta_time(&self->time_a, &self->time_b) < 1.0f / CPU_FPS) {
        clock_gettime(CLOCK_MONOTONIC, &self->time_b);
    }
    clock_gettime(CLOCK_MONOTONIC, &self->time_a);

    u8 i;
    for (i = 0; i < self->speed; i ++) {
        if (!self->paused) {
            self->fetch(self);
            self->execute(self);
        }
    }

    if (!self->paused) self->update_timers(self);
    if (self->sound_timer > 0) {
        self->speaker->play(self->speaker);
    } else {
        self->speaker->stop(self->speaker);
    }
    self->renderer->render_buffer(self->renderer);
}

void cpu_fetch(cpu_t* self) {
    self->opcode = (self->memory[self->pc] << 8) | (self->memory[self->pc + 1]);
    self->pc += 2;

    self->vx = (self->opcode & 0x0F00) >> 8;
    self->vy = (self->opcode & 0x00F0) >> 4;
}

void cpu_execute(cpu_t* self) {
    self->OP_table[(self->opcode & 0xF000) >> 12](self);
}

void cpu_update_timers(cpu_t* self) {
    if (self->delay_timer > 0) self->delay_timer -= 1;
    if (self->sound_timer > 0) self->sound_timer -= 1;
}

cpu_t* cpu_new(renderer_t* renderer, input_t* input, speaker_t* speaker) {
    cpu_t* cpu = (cpu_t*)malloc(sizeof(cpu_t)); {
        ZEROS(cpu->memory, MEMORY_SIZE);
        ZEROS(cpu->reg, REGISTER_SIZE);

        cpu->index = 0;
        cpu->pc = 0x200;

        cpu->delay_timer = 0;
        cpu->sound_timer = 0;

        ZEROS(cpu->stack, STACK_SIZE);
        cpu->sp = 0;

        cpu->speed = CPU_SPEED;

        clock_gettime(CLOCK_MONOTONIC, &(cpu->time_a));
        clock_gettime(CLOCK_MONOTONIC, &(cpu->time_b));

        cpu->paused = false;

        srand(time(NULL));

        cpu->renderer = renderer;
        cpu->input = input;
        cpu->speaker = speaker;

        cpu->load_font_sprites = cpu_load_font_sprites;
        cpu->load_program      = cpu_load_program;
        cpu->init_OP_tables    = cpu_init_OP_tables;
        cpu->cycle             = cpu_cycle;
        cpu->fetch             = cpu_fetch;
        cpu->execute           = cpu_execute;
        cpu->update_timers     = cpu_update_timers;
    }

    cpu->init_OP_tables(cpu);

    return cpu;
}

void cpu_delete(cpu_t** cpu) {
    free(*cpu);
}
