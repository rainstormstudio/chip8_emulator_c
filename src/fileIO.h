#ifndef FILEIO_H
#define FILEIO_H

#include "utilities.h"

Array(u8)* read_rom(cstring file_name);
void print_program(Array(u8)* program);

#endif // FILEIO_H
