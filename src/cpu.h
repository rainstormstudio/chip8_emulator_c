#ifndef CPU_H
#define CPU_H

#include "utilities.h"
#include "renderer.h"
#include "speaker.h"
#include "input.h"

#define MEMORY_SIZE   4096
#define REGISTER_SIZE 16
#define STACK_SIZE    16
#define FONTS_SIZE    80
#define CPU_SPEED     12
#define SPRITE_WIDTH  8
#define SPRITE_SIZE   5
#define CPU_FPS       60

#define OP_TABLE0_SIZE 0x10
#define OP_TABLE8_SIZE 0x10
#define OP_TABLEE_SIZE 0x10
#define OP_TABLEF_SIZE 0x66
#define OP_TABLE_SIZE  0x10

struct _cpu_t;
typedef void (*OP_func)(struct _cpu_t*);

STRUCT(cpu_t) {
    u8 memory[MEMORY_SIZE];
    u8 reg[REGISTER_SIZE];

    u16 index;
    u16 pc;

    u8 delay_timer;
    u8 sound_timer;

    u16 stack[STACK_SIZE];
    u16 sp;

    u16 opcode;
    u8 vx, vy;

    u8 speed;

    struct timespec time_a;
    struct timespec time_b;

    bool paused;

    renderer_t* renderer;
    input_t* input;
    speaker_t* speaker;

    OP_func OP_table0[OP_TABLE0_SIZE];
    OP_func OP_table8[OP_TABLE8_SIZE];
    OP_func OP_tableE[OP_TABLEE_SIZE];
    OP_func OP_tableF[OP_TABLEF_SIZE];
    OP_func OP_table[OP_TABLE_SIZE];

    void (*load_font_sprites)(cpu_t* self);
    void (*load_program)(cpu_t* self, Array(u8)* program);
    void (*init_OP_tables)(cpu_t* self);
    void (*cycle)(cpu_t* self);

    void (*fetch)(cpu_t* self);
    void (*execute)(cpu_t* self);
    void (*update_timers)(cpu_t* self);
};

cpu_t* cpu_new(renderer_t* renderer, input_t* input, speaker_t* speaker);
void cpu_delete(cpu_t** cpu);

// opcodes

inline static void OP_NULL(cpu_t* cpu);  // NULL
inline static void OP_00E0(cpu_t* cpu);  // CLS
inline static void OP_00EE(cpu_t* cpu);  // RET
inline static void OP_1nnn(cpu_t* cpu);  // JP addr
inline static void OP_2nnn(cpu_t* cpu);  // CALL addr
inline static void OP_3xkk(cpu_t* cpu);  // SE Vx, byte
inline static void OP_4xkk(cpu_t* cpu);  // SNE Vx, byte
inline static void OP_5xy0(cpu_t* cpu);  // SE Vx, Vy
inline static void OP_6xkk(cpu_t* cpu);  // LD Vx, byte
inline static void OP_7xkk(cpu_t* cpu);  // ADD Vx, byte
inline static void OP_8xy0(cpu_t* cpu);  // LD Vx, Vy
inline static void OP_8xy1(cpu_t* cpu);  // OR Vx, Vy
inline static void OP_8xy2(cpu_t* cpu);  // AND Vx, Vy
inline static void OP_8xy3(cpu_t* cpu);  // XOR Vx, Vy
inline static void OP_8xy4(cpu_t* cpu);  // ADD Vx, Vy
inline static void OP_8xy5(cpu_t* cpu);  // SUB Vx, Vy
inline static void OP_8xy6(cpu_t* cpu);  // SHR Vx {, Vy}
inline static void OP_8xy7(cpu_t* cpu);  // SUBN Vx, Vy
inline static void OP_8xyE(cpu_t* cpu);  // SHL Vx {, Vy}
inline static void OP_9xy0(cpu_t* cpu);  // SNE Vx, Vy
inline static void OP_Annn(cpu_t* cpu);  // LD I, addr
inline static void OP_Bnnn(cpu_t* cpu);  // JP V0, addr
inline static void OP_Cxkk(cpu_t* cpu);  // RND Vx, byte
inline static void OP_Dxyn(cpu_t* cpu);  // DRW Vx, Vy, nibble
inline static void OP_Ex9E(cpu_t* cpu);  // SKP Vx
inline static void OP_ExA1(cpu_t* cpu);  // SKNP Vx
inline static void OP_Fx07(cpu_t* cpu);  // LD Vx, DT
inline static void OP_Fx0A(cpu_t* cpu);  // LD Vx, K
inline static void OP_Fx15(cpu_t* cpu);  // LD DT, Vx
inline static void OP_Fx18(cpu_t* cpu);  // LD ST, Vx
inline static void OP_Fx1E(cpu_t* cpu);  // ADD I, Vx
inline static void OP_Fx29(cpu_t* cpu);  // LD F, Vx
inline static void OP_Fx33(cpu_t* cpu);  // LD B, Vx
inline static void OP_Fx55(cpu_t* cpu);  // LD [I], Vx
inline static void OP_Fx65(cpu_t* cpu);  // LD Vx, [I]

inline static void table0(cpu_t* cpu);
inline static void table8(cpu_t* cpu);
inline static void tableE(cpu_t* cpu);
inline static void tableF(cpu_t* cpu);

void OP_NULL(cpu_t* cpu) {}

void OP_00E0(cpu_t* cpu) {
    cpu->renderer->clear_buffer(cpu->renderer);
}

void OP_00EE(cpu_t* cpu) {
    cpu->sp -= 1;
    cpu->pc = cpu->stack[cpu->sp];
}

void OP_1nnn(cpu_t* cpu) {
    cpu->pc = (cpu->opcode & 0xFFF);
}

void OP_2nnn(cpu_t* cpu) {
    cpu->stack[cpu->sp] = cpu->pc;
    cpu->sp += 1;
    cpu->pc = (cpu->opcode & 0xFFF);
}

void OP_3xkk(cpu_t* cpu) {
    if (cpu->reg[cpu->vx] == (cpu->opcode & 0xFF)) {
        cpu->pc += 2;
    }
}

void OP_4xkk(cpu_t* cpu) {
    if (cpu->reg[cpu->vx] != (cpu->opcode & 0xFF)) {
        cpu->pc += 2;
    }
}

void OP_5xy0(cpu_t* cpu) {
    if (cpu->reg[cpu->vx] == cpu->reg[cpu->vy]) {
        cpu->pc += 2;
    }
}

void OP_6xkk(cpu_t* cpu) {
    cpu->reg[cpu->vx] = (cpu->opcode & 0xFF);
}

void OP_7xkk(cpu_t* cpu) {
    cpu->reg[cpu->vx] += (cpu->opcode & 0xFF);
}

void OP_8xy0(cpu_t* cpu) {
    cpu->reg[cpu->vx] = cpu->reg[cpu->vy];
}

void OP_8xy1(cpu_t* cpu) {
    cpu->reg[cpu->vx] |= cpu->reg[cpu->vy];
}

void OP_8xy2(cpu_t* cpu) {
    cpu->reg[cpu->vx] &= cpu->reg[cpu->vy];
}

void OP_8xy3(cpu_t* cpu) {
    cpu->reg[cpu->vx] ^= cpu->reg[cpu->vy];
}

void OP_8xy4(cpu_t* cpu) {
    u16 sum = (u16)(cpu->reg[cpu->vx]) + (u16)(cpu->reg[cpu->vy]);
    cpu->reg[0xF] = (sum > 0xFF) ? 1 : 0;
    cpu->reg[cpu->vx] = (u8)sum;
}

void OP_8xy5(cpu_t* cpu) {
    cpu->reg[0xF] = (cpu->reg[cpu->vx] > cpu->reg[cpu->vy]) ? 1 : 0;
    cpu->reg[cpu->vx] -= cpu->reg[cpu->vy];
}

void OP_8xy6(cpu_t* cpu) {
    cpu->reg[0xF] = (cpu->reg[cpu->vx] & 0x1);
    cpu->reg[cpu->vx] >>= 1;
}

void OP_8xy7(cpu_t* cpu) {
    cpu->reg[0xF] = (cpu->reg[cpu->vy] > cpu->reg[cpu->vx]) ? 1 : 0;
    cpu->reg[cpu->vx] = cpu->reg[cpu->vy] - cpu->reg[cpu->vx];
}

void OP_8xyE(cpu_t* cpu) {
    cpu->reg[0xF] = (cpu->reg[cpu->vx] & 0x80);
    cpu->reg[cpu->vx] <<= 1;
}

void OP_9xy0(cpu_t* cpu) {
    if (cpu->reg[cpu->vx] != cpu->reg[cpu->vy])
        cpu->pc += 2;
}

void OP_Annn(cpu_t* cpu) {
    cpu->index = (cpu->opcode & 0xFFF);
}

void OP_Bnnn(cpu_t* cpu) {
    cpu->pc = (cpu->opcode & 0xFFF) + cpu->reg[0];
}

void OP_Cxkk(cpu_t* cpu) {
    u8 random = (u8)rand();
    cpu->reg[cpu->vx] = random & (cpu->opcode & 0xFF);
}

void OP_Dxyn(cpu_t* cpu) {
    u8 width = SPRITE_WIDTH;
    u8 height = (cpu->opcode & 0xF);

    cpu->reg[0xF] = 0;

    u8 row, col;
    for (row = 0; row < height; row ++) {
        u8 sprite = cpu->memory[cpu->index + row];

        for (col = 0; col < width; col ++) {
            if ((sprite & 0x80) > 0) {
                if (cpu->renderer->set_pixel(
                        cpu->renderer,
                        cpu->reg[cpu->vx] + col,
                        cpu->reg[cpu->vy] + row))
                {
                    cpu->reg[0xF] = 1;
                }
            }

            sprite <<= 1;
        }
    }
}

void OP_Ex9E(cpu_t* cpu) {
    if (cpu->input->key_pressed(cpu->input, cpu->reg[cpu->vx])) {
        cpu->pc += 2;
    }
}

void OP_ExA1(cpu_t* cpu) {
    if (!cpu->input->key_pressed(cpu->input, cpu->reg[cpu->vx])) {
        cpu->pc += 2;
    }
}

void OP_Fx07(cpu_t* cpu) {
    cpu->reg[cpu->vx] = cpu->delay_timer;
}

void OP_Fx0A(cpu_t* cpu) {
    cpu->paused = true;
    if (!cpu->input->has_input) {
        cpu->pc -= 2;
    }
}

void OP_Fx15(cpu_t* cpu) {
    cpu->delay_timer = cpu->reg[cpu->vx];
}

void OP_Fx18(cpu_t* cpu) {
    cpu->sound_timer = cpu->reg[cpu->vx];
}

void OP_Fx1E(cpu_t* cpu) {
    cpu->index += cpu->reg[cpu->vx];
}

void OP_Fx29(cpu_t* cpu) {
    cpu->index = cpu->reg[cpu->vx] * SPRITE_SIZE;
}

void OP_Fx33(cpu_t* cpu) {
    cpu->memory[cpu->index + 0] = cpu->reg[cpu->vx] / 100;
    cpu->memory[cpu->index + 1] = (cpu->reg[cpu->vx] % 100) / 10;
    cpu->memory[cpu->index + 2] = cpu->reg[cpu->vx] % 10;
}

void OP_Fx55(cpu_t* cpu) {
    u8 idx;
    for (idx = 0; idx <= cpu->vx; idx ++) {
        cpu->memory[cpu->index + idx] = cpu->reg[idx];
    }
}

void OP_Fx65(cpu_t* cpu) {
    u8 idx;
    for (idx = 0; idx <= cpu->vx; idx ++) {
        cpu->reg[idx] = cpu->memory[cpu->index + idx];
    }
}

void table0(cpu_t* cpu) {
    cpu->OP_table0[cpu->opcode & 0xF](cpu);
}

void table8(cpu_t* cpu) {
    cpu->OP_table8[cpu->opcode & 0xF](cpu);
}

void tableE(cpu_t* cpu) {
    cpu->OP_tableE[cpu->opcode & 0xF](cpu);
}

void tableF(cpu_t* cpu) {
    cpu->OP_tableF[cpu->opcode & 0xFF](cpu);
}

#endif // CPU_H
