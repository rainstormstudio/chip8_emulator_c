#ifndef SPEAKER_H
#define SPEAKER_H

#include <SFML/Audio.h>

#include "utilities.h"

#define NUM_SAMPLES 100
#define SAMPLE_FREQUENCY 440.0f
#define SAMPLE_RATE 44100
#define SAMPLE_AMPLITUDE 10000

STRUCT(speaker_t) {
    sfSoundBuffer* buffer;
    sfSound* sound;
    i16 samples[NUM_SAMPLES];

    void (*play)(speaker_t* self);
    void (*stop)(speaker_t* self);
};

speaker_t* speaker_new();
void speaker_delete(speaker_t** speaker);

#endif // SPEAKER_H
