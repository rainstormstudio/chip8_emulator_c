#ifndef RENDERER_H
#define RENDERER_H

#include "utilities.h"
#include <SFML/Graphics.h>

#define TITLE "Chip-8 Emulator"

#define INNER_WIDTH 64
#define INNER_HEIGHT 32
#define ON_COLOR sfWhite
#define OFF_COLOR sfBlack

STRUCT(renderer_t) {
    i32 screen_width;
    i32 screen_height;
    i32 scale;

    sfRenderWindow* window;
    sfTexture*      screen_buffer;
    sfSprite*       screen_sprite;
    bool*           screen;
    u8*             pixels;

    bool (*set_pixel)(renderer_t* self, i32 x, i32 y);
    bool (*window_is_open)(renderer_t* self);
    void (*clear_buffer)(renderer_t* self);
    void (*render_buffer)(renderer_t* self);
};

inline static bool renderer_window_is_open(renderer_t* self) {
    return sfRenderWindow_isOpen(self->window);
}

renderer_t* renderer_new(i32 scale);
void renderer_delete(renderer_t** renderer);

#endif // RENDERER_H
