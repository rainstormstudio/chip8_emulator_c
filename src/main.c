#include "fileIO.h"
#include "renderer.h"
#include "input.h"
#include "speaker.h"
#include "cpu.h"

i32 main(i32 argc, cstring argv[]) {
    cstring rom_file = "";
    i32 scale = 10;

    size_t i;
    for (i = 1; i < argc; i ++) {
        if (strcmp(argv[i], "--scale") == 0) {
            if (i + 1 < argc) {
                i += 1;
                scale = atoi(argv[i]);
            } else {
                fprintf(stderr, "missing value after --scale\n");
                return EXIT_FAILURE;
            }
        } else {
            rom_file = argv[i];
        }
    }

    RAII(array_delete(u8)) Array(u8)* program = read_rom(rom_file);
    print_program(program);

    RAII(renderer_delete) renderer_t* renderer = renderer_new(scale);
    RAII(input_delete) input_t* input = input_new(renderer->window);
    RAII(speaker_delete) speaker_t* speaker = speaker_new();
    RAII(cpu_delete) cpu_t* cpu = cpu_new(renderer, input, speaker);
    cpu->load_font_sprites(cpu);
    cpu->load_program(cpu, program);

    while (renderer->window_is_open(renderer)) {
        input->update(input);
        cpu->cycle(cpu);
    }

    return EXIT_SUCCESS;
}
