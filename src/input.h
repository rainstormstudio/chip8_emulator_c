#ifndef INPUT_H
#define INPUT_H

#include <SFML/Graphics.h>

#include "utilities.h"

/*
Keyboard definition

Keypad       Keyboard
+-+-+-+-+    +-+-+-+-+
|1|2|3|C|    |1|2|3|4|
+-+-+-+-+    +-+-+-+-+
|4|5|6|D|    |Q|W|E|R|
+-+-+-+-+ => +-+-+-+-+
|7|8|9|E|    |A|S|D|F|
+-+-+-+-+    +-+-+-+-+
|A|0|B|F|    |Z|X|C|V|
+-+-+-+-+    +-+-+-+-+
*/

#define KEYPAD_SIZE 16 + 1

enum KeyPad {
    KEY_1 = 1,
    KEY_2,
    KEY_3,
    KEY_4,
    KEY_Q,
    KEY_W,
    KEY_E,
    KEY_R,
    KEY_A,
    KEY_S,
    KEY_D,
    KEY_F,
    KEY_Z,
    KEY_X,
    KEY_C,
    KEY_V
};

STRUCT(input_t) {
    sfRenderWindow* window;
    sfEvent event;
    bool has_input;
    bool keys[KEYPAD_SIZE];
    void (*update)(input_t* self);
    bool (*key_pressed)(input_t* self, u8 idx);
};

inline static bool input_key_pressed(input_t* self, u8 idx) {
    return self->keys[idx];
}

input_t* input_new(sfRenderWindow* window);
void input_delete(input_t** input);

#endif // INPUT_H
