#include "renderer.h"

bool renderer_set_pixel(renderer_t* self, i32 x, i32 y) {
    if (x >= INNER_WIDTH) x -= INNER_WIDTH;
    else if (x < 0) x += INNER_WIDTH;
    if (y >= INNER_HEIGHT) y -= INNER_HEIGHT;
    else if (y < 0) y += INNER_HEIGHT;

    i32 idx = y * INNER_WIDTH + x;
    self->screen[idx] = !self->screen[idx];

    return !self->screen[idx];
}

void renderer_clear_buffer(renderer_t* self) {
    sfRenderWindow_clear(self->window, sfBlack);

    i32 x, y;
    for (y = 0; y < INNER_HEIGHT; y ++) {
        for (x = 0; x < INNER_WIDTH; x ++) {
            i32 idx = y * INNER_WIDTH + x;
            self->screen[idx] = false;
        }
    }
}

void renderer_render_buffer(renderer_t* self) {
    i32 x, y;

    for (y = 0; y < INNER_HEIGHT; y ++) {
        for (x = 0; x < INNER_WIDTH; x ++) {
            i32 idx = y * INNER_WIDTH + x;
            switch (self->screen[idx]) {
            case true: {
                self->pixels[idx * 4 + 0] = ON_COLOR.r;
                self->pixels[idx * 4 + 1] = ON_COLOR.g;
                self->pixels[idx * 4 + 2] = ON_COLOR.b;
                self->pixels[idx * 4 + 3] = ON_COLOR.a;
                break;
            }
            case false: {
                self->pixels[idx * 4 + 0] = OFF_COLOR.r;
                self->pixels[idx * 4 + 1] = OFF_COLOR.g;
                self->pixels[idx * 4 + 2] = OFF_COLOR.b;
                self->pixels[idx * 4 + 3] = OFF_COLOR.a;
                break;
            }
            }
        }
    }

    sfTexture_updateFromPixels(self->screen_buffer, self->pixels, INNER_WIDTH, INNER_HEIGHT, 0, 0);
    sfRenderWindow_drawSprite(self->window, self->screen_sprite, NULL);
    sfRenderWindow_display(self->window);
}

renderer_t* renderer_new(i32 scale) {
    renderer_t* renderer = NEW(renderer_t); {
        renderer->screen_width = INNER_WIDTH * scale;
        renderer->screen_height = INNER_HEIGHT * scale;
        renderer->scale = scale;

        renderer->window = sfRenderWindow_create(
            (sfVideoMode){renderer->screen_width, renderer->screen_height, 32},
            TITLE, sfTitlebar | sfClose, NULL);
        sfRenderWindow_setVerticalSyncEnabled(renderer->window, sfTrue);
        sfRenderWindow_setFramerateLimit(renderer->window, 60);

        renderer->screen = (bool*)malloc(
            sizeof(bool) *
            INNER_WIDTH * INNER_HEIGHT);
        ARRAY_SET_ALL(renderer->screen, INNER_WIDTH * INNER_HEIGHT, false);
        renderer->pixels = (u8*)malloc(
            sizeof(u8) *
            INNER_WIDTH * INNER_HEIGHT * 4);
        ARRAY_SET_ALL(renderer->pixels, INNER_WIDTH * INNER_HEIGHT * 4, 0);
        renderer->screen_buffer = sfTexture_create(INNER_WIDTH, INNER_HEIGHT);
        renderer->screen_sprite = sfSprite_create();
        sfSprite_setTexture(renderer->screen_sprite, renderer->screen_buffer, sfTrue);
        sfSprite_setScale(renderer->screen_sprite, (sfVector2f){scale, scale});

        renderer->set_pixel = renderer_set_pixel;
        renderer->clear_buffer = renderer_clear_buffer;
        renderer->render_buffer = renderer_render_buffer;
        renderer->window_is_open = renderer_window_is_open;
    }

    return renderer;
}

void renderer_delete(renderer_t** renderer) {
    sfSprite_destroy((*renderer)->screen_sprite);
    sfTexture_destroy((*renderer)->screen_buffer);
    sfRenderWindow_destroy((*renderer)->window);
    free((*renderer)->pixels);
    free((*renderer)->screen);
    free(*renderer);
}
