#ifndef ARRAY_H
#define ARRAY_H

#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>

#define MIN_ARRAY_CAPACITY 32

#define define_array(type)                                              \
                                                                        \
    static void array_##type##_copy(                                    \
        type* dst, const type* src, size_t size)                        \
    {                                                                   \
        if (size == 0) return;                                          \
        size_t i;                                                       \
        for (i = 0; i < size; i ++) {                                   \
            dst[i] = src[i];                                            \
        }                                                               \
    }                                                                   \
                                                                        \
    typedef struct _array_##type Array_##type;                          \
                                                                        \
    struct _array_##type {                                              \
        type* data;                                                     \
        size_t size;                                                    \
        size_t capacity;                                                \
                                                                        \
        bool (*is_empty)(Array_##type* self);                           \
        type (*get)(Array_##type* self, size_t idx);                    \
        void (*set)(Array_##type* self, size_t idx, type item);         \
        void (*push_back)(Array_##type* self, type item);               \
        type (*pop_last)(Array_##type* self);                           \
        void (*reallocate)(Array_##type* self, size_t size);            \
        void (*resize)(Array_##type* self, size_t size);                \
    };                                                                  \
                                                                        \
    inline static bool array_##type##_is_empty(Array_##type* self) {    \
        return self->size == 0;                                         \
    }                                                                   \
                                                                        \
    inline static type array_##type##_get(                              \
        Array_##type* self, size_t idx)                                 \
    {                                                                   \
        return self->data[idx];                                         \
    }                                                                   \
                                                                        \
    inline static void array_##type##_set(                              \
        Array_##type* self, size_t idx, type item)                      \
    {                                                                   \
        self->data[idx] = item;                                         \
    }                                                                   \
                                                                        \
    inline static void array_##type##_push_back(                        \
        Array_##type* self, type item)                                  \
    {                                                                   \
        size_t old_size = self->size;                                   \
        self->resize(self, old_size + 1);                               \
        self->data[old_size] = item;                                    \
    }                                                                   \
                                                                        \
    inline static type array_##type##_pop_last(Array_##type* self) {    \
        self->size -= 1;                                                \
        return self->data[self->size];                                  \
    }                                                                   \
                                                                        \
    inline static void array_##type##_reallocate(                       \
        Array_##type* self, size_t size)                                \
    {                                                                   \
        type* new_data = NULL;                                          \
        if (size > 0) {                                                 \
            new_data = (type*)malloc(sizeof(type) * size);              \
            array_##type##_copy(new_data, self->data,                   \
                                size < self->size ? size : self->size); \
        }                                                               \
        free(self->data);                                               \
        self->data = new_data;                                          \
        self->capacity = size;                                          \
    }                                                                   \
                                                                        \
    inline static void array_##type##_resize(                           \
        Array_##type* self, size_t size)                                \
    {                                                                   \
        if (size > self->capacity) {                                    \
            size_t alloc_size = (size_t)                                \
                (MIN_ARRAY_CAPACITY / sizeof(type));                    \
            alloc_size = alloc_size > 1 ? alloc_size : 1;               \
            while (size > alloc_size) alloc_size <<= 1;                 \
            self->reallocate(self, alloc_size);                         \
        }                                                               \
        self->size = size;                                              \
    }                                                                   \
                                                                        \
    static Array_##type* array_##type##_new() {                         \
        Array_##type* ary = (Array_##type*)                             \
            malloc(sizeof(Array_##type));                               \
        {                                                               \
            ary->data       = NULL;                                     \
            ary->size       = 0;                                        \
            ary->capacity   = 0;                                        \
            ary->is_empty   = array_##type##_is_empty;                  \
            ary->get        = array_##type##_get;                       \
            ary->set        = array_##type##_set;                       \
            ary->push_back  = array_##type##_push_back;                 \
            ary->pop_last   = array_##type##_pop_last;                  \
            ary->reallocate = array_##type##_reallocate;                \
            ary->resize     = array_##type##_resize;                    \
        }                                                               \
        return ary;                                                     \
    }                                                                   \
                                                                        \
    static void array_##type##_delete(Array_##type** ary) {             \
        free((*ary)->data);                                             \
        free(*ary);                                                     \
    }                                                                   \

#define Array(type) Array_##type
#define array_new(type) array_##type##_new
#define array_delete(type) array_##type##_delete

#endif // ARRAY_H
