#ifndef UTILITIES_H
#define UTILITIES_H

#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include <time.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

#include "array.h"

typedef int8_t   i8;
typedef int16_t  i16;
typedef int32_t  i32;
typedef int64_t  i64;

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef float    f32;
typedef double   f64;

typedef char*    cstring;

define_array(i16);
define_array(i32);
define_array(i64);

define_array(u8);
define_array(u16);
define_array(u32);
define_array(u64);

define_array(f32);
define_array(f64);

define_array(bool);
define_array(char);
define_array(cstring);

#define RAII(func) __attribute__((cleanup(func)))

#define NEW(type) (type*)malloc(sizeof(type))

#define STRUCT(name)                            \
    typedef struct _##name name;                \
    struct _##name

#define ZEROS(ary, size) {                      \
        size_t i;                               \
        for (i = 0; i < size; i ++) {           \
            ary[i] = 0;                         \
        }                                       \
    }                                           \

#define ARRAY_SET_ALL(ary, size, item) {        \
        size_t i;                               \
        for (i = 0; i < size; i ++) {           \
            ary[i] = item;                      \
        }                                       \
    }                                           \

#endif // UTILITIES_H
