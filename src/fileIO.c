#include "fileIO.h"

Array(u8)* read_rom(cstring file_name) {
    if (strlen(file_name) == 0) {
        fprintf(stderr, "no rom file specified\n");
        exit(EXIT_FAILURE);
    }

    Array(u8)* program = array_new(u8)();

    FILE* file = fopen(file_name, "rb");
    if (!file) {
        fprintf(stderr, "Unable to open rom file\n");
        exit(EXIT_FAILURE);
    }

    i32 byte;
    while ((byte = getc(file)) != EOF) {
        program->push_back(program, (u8)byte);
    }

    return program;
}

void print_program(Array(u8)* program) {
    size_t i;
    for (i = 0; i < program->size; i ++) {
        if (i != 0 && i % 16 == 0) printf("\n");
        fprintf(stderr, "%02X ", program->data[i]);
    }
    fprintf(stderr, "\n");
}
