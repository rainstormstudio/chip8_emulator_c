#include "speaker.h"

static void speaker_play(speaker_t* self) {
    if (sfSound_getStatus(self->sound) == sfStopped) {
        sfSound_play(self->sound);
    }
}

static void speaker_stop(speaker_t* self) {
    if (sfSound_getStatus(self->sound) == sfPlaying) {
        sfSound_stop(self->sound);
    }
}

speaker_t* speaker_new() {
    speaker_t* speaker = (speaker_t*)malloc(sizeof(speaker_t)); {
        i32 i;
        for (i = 0; i < NUM_SAMPLES; i ++) {
            speaker->samples[i] = SAMPLE_AMPLITUDE * sinf(SAMPLE_FREQUENCY * (2.0f * M_PI) * i / SAMPLE_RATE);
        }
        speaker->buffer = sfSoundBuffer_createFromSamples(speaker->samples, NUM_SAMPLES, 2, SAMPLE_RATE);
        speaker->sound = sfSound_create();
        sfSound_setBuffer(speaker->sound, speaker->buffer);
        sfSound_setLoop(speaker->sound, true);

        speaker->play = speaker_play;
        speaker->stop = speaker_stop;
    }
    return speaker;
}

void speaker_delete(speaker_t** speaker) {
    sfSound_destroy((*speaker)->sound);
    sfSoundBuffer_destroy((*speaker)->buffer);
    free(*speaker);
}
